using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraController : MonoBehaviour
{
    public GameObject PlayerNinja;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        var x = PlayerNinja.transform.position.x + 8;
        var y = PlayerNinja.transform.position.y ;
        transform.position = new Vector3(x,y,transform.position.z);
    }
}
