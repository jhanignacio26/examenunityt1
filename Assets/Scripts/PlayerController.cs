using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    
    private bool salta = false;
    private SpriteRenderer sr;
    private Animator _animator;
    private Rigidbody2D rb2d;
    
    public float speed = 15;
    public float upSpeed = 50;
    
    private bool muere = false;
    private int masVelocidad = 0;
    void Start()
    {
        sr = GetComponent<SpriteRenderer>(); 
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKey(KeyCode.Space) && salta && !muere)
        {
            setJumpAnimation();
            rb2d.velocity = Vector2.up * upSpeed;
            salta = false;
        }
        else if (!muere)
        {
            setRunAnimation();
            rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
            if (masVelocidad == 10)
            {
                speed += 5;
                masVelocidad = 0;
            }
        }

        if (muere)
        {
            setDeadAnimation();
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
        }
    }
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        salta = true;
        if (other.gameObject.layer == 6)
        {
            setDeadAnimation();
        }
        
        if (other.gameObject.tag == "Enemy")
        {
            muere = true;
        }
        
    }

    //Velocidad
    private void OnTriggerEnter2D(Collider2D other)
    {
        salta = true;
        if (other.gameObject.name == "GameObject")
        {
            masVelocidad += 1;
        }
        
    }

    private void setDeadAnimation()
    {
        _animator.SetInteger("Estado",3);
    }
    private void setJumpAnimation()
    {
        _animator.SetInteger("Estado",2);
    }
    private void setRunAnimation()
    {
        _animator.SetInteger("Estado", value:1);
    }
    
}
