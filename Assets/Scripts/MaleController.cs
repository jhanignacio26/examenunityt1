using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaleController : MonoBehaviour
{
    // Start is called before the first frame update
    private Animator _animator;
    private Rigidbody2D rb2d;
    
    void Start()
    {
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        setAttack();
        rb2d.velocity = new Vector2(-8, rb2d.velocity.y);
    }

    private void setAttack()
    {
        _animator.SetInteger("Estado",0);
    }
}
